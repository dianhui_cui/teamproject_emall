<?php
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->map(['GET','POST'],'/admin/categories/{action:list|add|edit|delete}[/{id:[0-9]+}]', function ($request, $response, $args) use ($app){
    $view = Twig::fromRequest($request);
    $action = $args['action'];
    //$name = isset($args['name'])? $args['name']:" ";
    if (!isset($_SESSION['user'])){
        $response = $response->withStatus(403);
        return $view->render($response, 'error_forbidden.html.twig');
    } else if ($_SESSION['user']['UserType']!== 'Admin') {
        $response = $response->withStatus(403);
        return $view->render($response, 'error_forbidden.html.twig');
    }
    if ($request->getMethod() == "GET") {
        /*if ($action == 'list') {
            return $view->render($response, 'categories_list.html.twig');
        }*/
        
        switch ($action){
            case 'list'  : {
                $categoryList = DB::query("SELECT * from productcategories WHERE parentId IS NULL;");
                return $view->render($response, 'categories_list.html.twig',['categoryList'=>$categoryList]);
            }
            case 'add'   : {
                $categoryList = DB::query("SELECT * from productcategories WHERE parentId is null;");
                return $view->render($response, 'category_add.html.twig',['categoryList'=>$categoryList]);
            }
            case 'edit'  : {
                $categoryId = isset($args['id'])? $args['id']:""; // deal with condition where there is no {name}
                // print_r($categoryId);
                $category = DB::queryFirstRow("SELECT * FROM productcategories WHERE id=%i", $categoryId);
                if (!$category) {
                    return $view->render($response, 'error_notfound.html.twig');
                }
                $categoryList = DB::query("SELECT * from productcategories WHERE parentId is null;");
                return $view->render($response, 'category_edit.html.twig',['categoryList'=>$categoryList, 'currCategory' => $category]);
            }
            case 'delete': {
                $categoryId = isset($args['id'])? $args['id']:""; // deal with condition where there is no {name}
                // print_r($categoryId);
                $category = DB::queryFirstRow("SELECT * FROM productcategories WHERE id=%i", $categoryId);
                if (!$category) {
                    return $view->render($response, 'error_notfound.html.twig');
                }
                return $view->render($response,'category_delete.html.twig', ['category' => $category]);
                
            }
            default      : return $view->render($response, 'error_notfound.html.twig');
        }
    }
    if ($request->getMethod() == "POST") {
        /*if ($action == 'list') {
            return $view->render($response, 'categories_list.html.twig');
        }*/
        switch ($action){
            case 'add'   : {
                $postVars = $request->getParsedBody();
                $name = $postVars['categoryname'];
                //$parentId = isset($postVars['comboCategory'])?$postVars['comboCategory']:null;
                $parentId = $postVars['comboCategory'];  
                print_r($parentId);
                $error = "";
                if (!preg_match('/^[^\/]{3,100}$/', $name)){
                    $error = "Error: category name $name is invalid format"; 

                } else { // check if category exist
                    $category = DB::queryFirstRow("SELECT * FROM productcategories WHERE name=%s", $name);
                    if ($category) {
                        $error = "Error: category name $name exists"; 
                    }
                }
                
                
                //
               if ($error){
                    return $view->render($response, 'category_add.html.twig',[
                         'error' => $error
                    ]);
                } else {
                    // Insert record to database using Meekrodb
                    if ($parentId == 0){
                        DB::insert('productcategories',  ['name' => $name]);
                    } else {
                        DB::insert('productcategories',  ['name' => $name, 'parentId' => $parentId]);
                    }
                    $categories = DB::query("SELECT * from productcategories WHERE parentId is null;");
                    return $view->render($response, 'categories_list.html.twig',['categoryList'=>$categories]);
                }

                
            }
            case 'delete': {
                $postVars = $request->getParsedBody();
                $categoryId = isset($args['id'])? $args['id']:""; 
                if ($postVars['confirmed'] == 'true') {
                    DB::query("DELETE FROM productcategories WHERE id=%d;", $categoryId);
                    $categoryList = DB::query("SELECT * from productcategories WHERE parentId IS NULL;");
                    // FIXME: AJAX delete / API delete will be better
                    return $view->render($response, 'categories_list.html.twig',['categoryList'=>$categoryList]);
                } else {
                    return $view->render($response, 'error_internal.html.twig');
                }
                
                // return $response;// $view->render($response, 'categories_list.html.twig',['categories'=>$categories]);
                // return $view->render($response, 'category_delete.html.twig');
                // return $response->withRedirect('/admin/categories/list', 301);
                // $app->redirect('/admin/categories/list', 301);
                
            }
            case 'edit': {
                $postVars = $request->getParsedBody();
                $categoryId = isset($args['id'])? $args['id']:""; // deal with condition where there is no {name}
                // print_r($categoryId);
                $category = DB::queryFirstRow("SELECT * FROM productcategories WHERE id=%i", $categoryId);
                if (!$category) {
                    return $view->render($response, 'error_notfound.html.twig');
                }

                $name = $postVars['categoryname'];
                //$parentId = isset($postVars['comboCategory'])?$postVars['comboCategory']:null;
                $parentId = $postVars['comboCategory'];  
                print_r($parentId);
                $error = "";
                if (!preg_match('/^[^\/]{3,100}$/', $name)){
                    $error = "Error: category name $name is invalid format"; 

                } else { // check if category exist
                    $count = DB::queryFirstField("SELECT COUNT(id) FROM productcategories WHERE name=%s", $name);
                    print_r($name);
                    print_r($count);
                    print_r($category['name']);
                    if ($count != 0 && $name != $category['name']) {
                        $error = "Error: category name $name exists, count is $count"; 
                    }
                }
                
                
                //
               if ($error){
                   
                    $categoryList = DB::query("SELECT * from productcategories WHERE parentId is null;");
                    return $view->render($response, 'category_edit.html.twig',['error' => $error, 'categoryList'=>$categoryList, 'currCategory' => $category]);
                    
                } else {
                    // Insert record to database using Meekrodb
                    if ($parentId == 0){
                        DB::update('productcategories',  ['name' => $name], "id=%i", $categoryId);
                    } else {
                        DB::update('productcategories',  ['name' => $name, 'parentId' => $parentId], "id=%i", $categoryId);
                    }
                    $categories = DB::query("SELECT * from productcategories WHERE parentId is null;");
                    return $view->render($response, 'categories_list.html.twig',['categoryList'=>$categories]);
                }
            }
            default      : return $view->render($response, 'error_notfound.html.twig');
        }
    }
});

$app->get('/ajax/loadSubCategories[/by/{id}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    
    $id = isset($args['id'])? $args['id']:""; // deal with condition where there is no {username}
    if (!$id){
        $categoryList = DB::query("SELECT * FROM productcategories ORDER BY id");
    } else {
        $categoryList = DB::query("SELECT * FROM productcategories WHERE parentId=%d ORDER BY id", $id);
    }
    return $view->render($response, 'category_list_load.html.twig', ['subCategoryList' => $categoryList]);
});
$app->get('/admin/iscategoryexist/[{categoryname}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $categoryname = isset($args['categoryname'])? $args['categoryname']:""; // deal with condition where there is no {name}
    //print_r($categoryname);
    $category = DB::queryFirstRow("SELECT id FROM productcategories WHERE name=%s", $categoryname);
    return $view->render($response, 'admin_iscategoryexist.html.twig', ['isExist' => ($category != null)]);
});
const CATEGORIES_PER_PAGE = 5;
$app->get('/ajax/categorylist/{parentId:[0-9]+}/{pageNo:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $pageNo = $args['pageNo'];
    $parentId = $args['parentId'];
    $totalCategories = 0;
    $totalPages = 0;
    $categorySkip = ($pageNo - 1) * CATEGORIES_PER_PAGE;
    if ($parentId == 0) {
        $totalCategories = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM productcategories");
        $subCategoryList = DB::query("SELECT * FROM productcategories ORDER BY id LIMIT %i,%i ", $categorySkip, CATEGORIES_PER_PAGE);    
    } else {
        $totalCategories = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM productcategories WHERE parentId = %d", $parentId);
        $subCategoryList = DB::query("SELECT * FROM productcategories WHERE parentId = %d ORDER BY id LIMIT %i,%i ", $parentId, $categorySkip, CATEGORIES_PER_PAGE);    
    }
    
    $totalPages = ceil($totalCategories / CATEGORIES_PER_PAGE);
    return $view->render($response, '/category_list_load.html.twig', [ 'subCategoryList' => $subCategoryList, 'totalPages' => $totalPages, 'parentId' => $parentId ]);
});
?>