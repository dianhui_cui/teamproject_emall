<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Views\Twig;

require_once "setup.php";
require_once "config.php";
$app->get('/register', function ($request, $response, $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'buyer_register.html.twig', [
    ]);
});

$app->post('/register', function ($request, $response, $args) {
    $view = Twig::fromRequest($request);
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();
    // print_r($uploadedFiles);
    // handle single input with single file upload
    // $uploadedFile = isset($uploadedFiles)? $uploadedFiles['image']:null;

    // extract submitted values
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $email = $postVars['email'];
    $pwd1 = $postVars['pwd1'];
    $pwd2 = $postVars['pwd2'];
    
    $errorsArray = array();
    // check validity
    if (!preg_match('/^[a-zA-Z0-9_]{3,20}$/', $username)){
        //if (!preg_match('/^[a-zA-Z][$/',$name)){
        array_push($errorsArray, "username must be composed of 5-20 characters only uppercase or lowercase letters and digits and underscore");
        $postVars['username'] = '';
    }  else { // check user is not registered yet
        $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
        if ($user) {
            array_push($errorsArray, "Username already registered, try a different one");
            $postVars['username'] = "";
        }
    }
    
    if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{5,100})/',$pwd1)){
        array_push($errorsArray, "Error: password must be composed of 5-100 characters with at least one uppercase, one lowercase and one digit."); 
        
    }
    if ($pwd1!=$pwd2){
        array_push($errorsArray, "passwords do not match");
    }
    
    if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $errorsArray[] = "Error: email is invalid format"; 
        // or: array_push($errorList, "Name must be 2-20 characters long.");
        $email = "";
    } else { // check email is not registered yet
        $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($user) {
            array_push($errorsArray, "email already registered, try a different one");
            $postVars['email'] = "";
        }
    }
    //
    if ($errorsArray){
        return $view->render($response, 'buyer_register.html.twig',[
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else {
        // Insert record to database using Meekrodb
        DB::insert('users', ['username' => $username,  'password'=>$pwd1, 'email' => $email, 'UserType' => "Buyer" ]);
        return $view->render($response, 'register_success.html.twig',[
            'v' => $postVars
        ]);
    }
});

$app->get('/login', function ($request, $response, $args) {
    global $gClient;
    $view = Twig::fromRequest($request);
    

	if (isset($_SESSION['access_token'])) {
		header('Location: /');
		exit();
	}

    $loginURL = $gClient->createAuthUrl();

    return $view->render($response, 'login.html.twig', ['loginURL' => $loginURL]);
});
$app->post('/login', function ($request, $response, $args) {
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    //====== ReCAPTCHA verification
    
    $secretKey = "6LcvdPgUAAAAAMn0_R3sP_fsDC23m4eet1QEvk7Y";
    $responseKey = $postVars['g-recaptcha-response'];
    $userIP = $_SERVER['REMOTE_ADDR'];

    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
    $response_recaptcha = file_get_contents($url);
    $response_recaptcha = json_decode($response_recaptcha);
    /* if ($response_recaptcha->success)
    {
        //echo "Verification success";
    }
    else
        {echo "Verification failed!";
        $recaptcha;}*/
    
    //===========end ReCAPTCHA verification
    $username = $postVars['username'];
    $pwd = $postVars['pwd'];
    // check validity
    $loginSuccessful = false;

    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
    if ($user) {
        if ($user['password'] == $pwd && $response_recaptcha->success) {
            $loginSuccessful = true;
        } else {
            $postVars['pwd']='';
        }
    }

    //
    if (!$loginSuccessful){
        return $view->render($response, 'login.html.twig',[
            'v' => $postVars, 'error' => true
        ]);
    } else {
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
        return $view->render($response, 'login_success.html.twig',[
            'v' => $postVars
        ]);
    }
});

$app->get('/logout', function (Request $request, Response $response, array $args) {
    global $gClient;
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']); 
    unset($_SESSION['access_token']);       
    unset($_SESSION['guser']); 
    // Reset OAuth access token
    $gClient->revokeToken();
 
    return $view->render($response, 'logout.html.twig');
});


$app->get('/register/isusernametaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    
    $username = isset($args['username'])? $args['username']:" "; // deal with condition where there is no {username}
    $user = DB::queryFirstRow("SELECT id FROM users WHERE username=%s", $username);
    return $view->render($response, 'register_isusernametaken.html.twig', ['isTaken' => ($user != null)]);
});
// for debugging purposes only
$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});
function moveUploadedFile($directory, UploadedFileInterface $uploadedFile,$filename)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    //$basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    //$filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}
?>