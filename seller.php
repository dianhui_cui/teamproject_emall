<?php
use Slim\Factory\AppFactory;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

require_once "setup.php";

$app->get('/seller', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
    }
    return $view->render($response, 'seller-index.html.twig',['seller'=>$seller]);
 
});
$app->get('/seller/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
    }
    if($seller)
    {
        return $response->withHeader('Location','/seller')->withStatus(302);
        
    }else
    {
        return $view->render($response, 'seller-login.html.twig');
      
    }
 
});

$app->post('/seller/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $password = $postVars['password'];
    
    $errorList=array();
    $seller = DB::queryFirstRow("SELECT * FROM users WHERE username=%s and usertype='Seller'", $username);
    if ($seller) 
    {
        if ($seller['password'] == $password) {
            
        }
        else
        {
            array_push($errorList,"Login failed by password".$password);
            $postVars['password']="";
        }
    }else
    {
        array_push($errorList,"Can't find user by userName:".$username);
        $postVars['username']="";

    }
    //
    if ($errorList) {        
        return $view->render($response, 'seller-login.html.twig', ['v' =>$postVars,'errorList'=> $errorList ]);
    } 
    else 
    {
        unset($seller['password']); // remove password from array for security reasons
        unset($seller['RegisterTime']);
        unset($seller['orderCount']);
      
        unset($seller['description']);
        unset($seller['shopPage']);
       
        $_SESSION['seller'] = $seller;
    
        return $response->withHeader('Location','/seller')->withStatus(302);
    }
 
});

$app->get('/seller/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
   
    if(isset($_SESSION['seller']))
    {
        $_SESSION['seller']=null;
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
   
});

$app->get('/seller/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
         return $view->render($response, 'seller-register.html.twig');
 });
 $app->post('/seller/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $email = $postVars['email'];
    $password = $postVars['pass1'];
    $confirm = $postVars['pass2'];
    $email= $postVars['email'];
    $firstname= $postVars['firstname'];
    $middlename= $postVars['middlename'];
    $lastname= $postVars['lastname'];
    $phone= $postVars['phone'];
    $address= $postVars['address'];
    $city= $postVars['city'];
    $province= $postVars['province'];
    $country= $postVars['country'];
    $postalcode= $postVars['postalcode'];
    $description= $postVars['description'];


    $errorList = array();
    if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/', $username) != 1) 
    {
        array_push($errorList, "Username must be 6-20 characters long, begin with a letter and only "
               . "consist of uppercase/lowercase letters, digits, and underscores");
        $postVars['username'] = '';
    } 
    else 
    { // check user is not registered yet
        $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
        if ($user) {
            array_push($errorList, "Username already registered, try a different one");
            $postVars['username'] = "";
        }
    }
    /*
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email does not look valid");
        $postVars['email'] = "";
    }
    */
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        array_push($errorList, "Email already registered, try a different one");
        $postVars['email'] = "";
    }
    if ($password != $confirm) {
        array_push($errorList, "Passwords do not match");        
    } else {
        if ((strlen($password) < 6)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
            array_push($errorList, "Password must be at least 6 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it");
        }
    } 
       
  
    if(strlen($firstname)<1||strlen($firstname>50))
    {
        array_push($errorList, "First name must be 1-50 charactors."); 
    }

    if(strlen($lastname)<1||strlen($lastname>50))
    {
        array_push($errorList, "Last name must be 1-50 charactors."); 
    }
    if(strlen($middlename)<0||strlen($lastname>10))
    {
        array_push($errorList, "Middle name must be 0-10 charactors."); 
    }
    //
    if ($errorList) {        
        return $view->render($response, 'seller-register.html.twig', ['v' =>$postVars,'errorList'=> $errorList ]);
    } 
    else 
    {
      //add user 
      
      DB::insert('users',['username'=>$username,'email'=>$email,'password'=>$password,'userType'=>'Seller','firstname'=>$firstname,'middlename'=>$middlename,'lastname'=>$lastname,'phoneNo'=>$phone,'address'=>$address,'city'=>$city,'province'=>$province,'country'=>$country,'postalcode'=>$postalcode,'description'=>$description,'orderCount'=>0,'score'=>0,'shopPage'=>'']);
      $id=DB::insertId();
      $user=DB::queryFirstRow("select * from users where id=%i0",$id);
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['seller'] = $user;
         return $response->withHeader('Location','/seller')->withStatus(302);
    }
    
    return $view->render($response, 'seller-register.html.twig');
 });

 $app->get('/seller/personal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
    }
     return $view->render($response, 'seller-personal-setting.html.twig',['seller'=>$seller]);
 });
 $app->get('/seller/account', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    $account=null;
    $cashflows=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $account=DB::queryFirstRow("select * from useraccounts where userId=".$seller['id']);
        if($account)
        {

            $cashflows=DB::query("select * from usercashflows where accountId=".$account['id']." order by operateTime desc");
        }
    }

     return $view->render($response, 'seller-cash-account.html.twig',['seller'=>$seller,'account'=>$account,'cashflows'=>$cashflows]);
 });
 $app->get('/seller/products', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $products=DB::query("select a.*,b.name as categoryName from products a, productcategories b where a.categoryId=b.id and a.sellerId=".$seller['id']);
        $categories=array();
        foreach($products as $product)
        {
            if (!array_key_exists($product['categoryId'],$categories))
            {
                $categories[$product['categoryId']]=$product['categoryName'];
            }
        }
        $statistics=array();//statistics categoryName,productCount
        foreach($products as $product)
        {
            if (!array_key_exists($product['categoryId'], $statistics))
            {
                $statistics[$product['categoryId']]=1;
            }
            else
            {
                $statistics[$product['categoryId']]+=1;
            }
        }
        $productArray=array();//category,products
        foreach($products as $product)
        {
            if (!array_key_exists($product['categoryId'], $productArray))
            {
                $productArray[$product['categoryId']]=array();
            }
                array_push($productArray[$product['categoryId']],$product);
           
         
        }

        return $view->render($response, 'seller-products.html.twig',['seller'=>$seller,'categories'=>$categories,'statistics'=>$statistics,'productArray'=>$productArray]);
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
 });
 $app->get('/seller/product/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $categories=DB::query("select * from productcategories");
        return $view->render($response, 'seller-product-add.html.twig',['seller'=>$seller,'categories'=> $categories]);
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
 });
 
 $app->post('/seller/product/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $directory = $this->get('upload_directory');
        $uploadedFiles = $request->getUploadedFiles();
        $postVars = $request->getParsedBody();
        $category = (int)$postVars['category'];
        $productname = $postVars['productname'];
        $price = (float)$postVars['price'];

        $inventory =  (float)$postVars['inventory'];
        $discount =  (float)$postVars['discount'];
        $description = $postVars['description'];
        $imagePath="";
        $errorList = array();
        if(!$errorList)
        {
        
            $uploadedFile = $uploadedFiles['picture'];
            
            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $filename = moveUploadedFileRandom($directory, $uploadedFile);
                $imagePath="/uploads/".$filename;
            } 
            else 
            {
                array_push($errorList, "Upload photo failed.");        
            }
        }
        if(!$imagePath)
        {
            array_push($errorList, "Photo is required.");
        }

        DB::insert("products",['categoryId'=> $category,'name'=>$productname,'sellerId'=> (int)$seller['id'],'unitPrice'=>$price,'picture'=>'','pictureUrl'=>$imagePath,'description'=>$description,'inventory'=>$inventory,'discount'=>$discount,'rating'=>0,'reviewCount'=>0,'salesCount'=>0]);
        return $response->withHeader('Location','/seller/products')->withStatus(302);
        
    }
    else
    {
        return $response->withHeader('Location','/seller')->withStatus(302);
    }
  
 });


 $app->get('/seller/product/update/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $id=$args['id'];
        $categories=DB::query("select * from productcategories");
        $product=DB::queryFirstRow("select * from products where id=".$id." and sellerId=".$seller['id']);
        return $view->render($response, 'seller-product-add.html.twig',['seller'=>$seller,'categories'=> $categories,'product'=>$product]);
    }
    return $response->withHeader('Location','/seller')->withStatus(302); 
 });
 $app->post('/seller/product/update/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $id=$args['id'];
        $directory = $this->get('upload_directory');
        $uploadedFiles = $request->getUploadedFiles();
        $postVars = $request->getParsedBody();
        $category = (int)$postVars['category'];
        $productname = $postVars['productname'];
        $price = (float)$postVars['price'];
        $inventory =  (float)$postVars['inventory'];
        $discount =  (float)$postVars['discount'];
        $description = $postVars['description'];
        $imagePath="";
        if($uploadedFiles)
        {
            $uploadedFile = $uploadedFiles['picture'];
            
            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $filename = moveUploadedFileRandom($directory, $uploadedFile);
                $imagePath="/uploads/".$filename;
            } 
            else 
            {
                array_push($errorList, "Upload photo failed.");        
            }
        }
        $errorList = array();
      

        
        if( $imagePath)
        {
            DB::update("products",['categoryId'=> $category,'name'=>$productname,'unitPrice'=>$price,'pictureUrl'=>$imagePath,'description'=>$description,'inventory'=>$inventory,'discount'=>$discount],"id=%i",$id);
        }
        else
        {
            DB::update("products",['categoryId'=> $category,'name'=>$productname,'unitPrice'=>$price,'description'=>$description,'inventory'=>$inventory,'discount'=>$discount],"id=%i",$id);
        }
         return $response->withHeader('Location','/seller/products')->withStatus(302);
        
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
 });

 $app->get('/seller/orders', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and a.sellerId=".$seller['id']." and a.orderStatus in ('placed','paid','shipped') order by a.orderTime desc");
        return $view->render($response, 'seller-orders-inprocess.html.twig',['seller'=>$seller,'orders'=> $orders ]);
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
 });
 $app->get('/seller/orderhistories', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and a.sellerId=".$seller['id']." and a.orderStatus not in ('placed','paid','shipped') order by a.orderTime desc");
        return $view->render($response, 'seller-orders-history.html.twig',['seller'=>$seller,'orders'=> $orders ]);
    }
    return $response->withHeader('Location','/seller')->withStatus(302);
 });
 $app->post('/seller/account/create', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {

        $seller=$_SESSION['seller'];
        $postVars= $request->getParsedBody();
        $accountName=$postVars['accountname'];
        $amount=$postVars['amount'];
        DB::insert("useraccounts",['userId'=>$seller['id'],'name'=>$accountName,'amount'=>$amount]);
        return $response->withHeader('Location','/seller/account')->withStatus(302);
    }
    else
    {
        return $response->withHeader('Location','/seller')->withStatus(302);
    }
 });

 $app->post('/seller/account/withdraw', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $postVars= $request->getParsedBody();
        $withdrawamount=$postVars['withdrawamount'];
        $owner=$postVars['owner'];
        $accountno=$postVars['accountno'];
        $bank=$postVars['bank'];
        $account=DB::queryFirstRow("select * from useraccounts where userId=".$seller["id"]);
        $amount=$account["amount"];
        $amount-=$withdrawamount;
        DB::update("useraccounts",['amount'=>$amount],"id=%i",$account["id"]);
        DB::insert("usercashflows",['accountId'=>$account['id'],'amount'=>$withdrawamount,'operateType'=>'withdraw']);
        return $response->withHeader('Location','/seller/account')->withStatus(302);
    }
    else
    {
        return $response->withHeader('Location','/seller')->withStatus(302);
    }
    
 });

 $app->get('/seller/performance/{condition}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $seller=null;
    $condition=$args['condition'];
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        return $view->render($response, 'seller-sales-performance.html.twig',['seller'=>$seller,'period'=>$condition]);
    }
    else
    {
        return $response->withHeader('Location','/seller')->withStatus(302);
    } 
 });



 $app->get('/api/seller/checkUserName/{id:[0-9]+}/{username}', function (Request $request, Response $response, array $args) {
    
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $id = $args['id'];
    $username=$args['username'];
    $userId=DB::queryFirstRow("select id from users where username=%s and id<>%i",$username,$id);
    if($userId)
    {
       $response->getBody()->write(json_encode(true));
    }else
    {
       $response->getBody()->write(json_encode(false));
    }
    return $response;
 });
 $app->get('/api/seller/checkEmail/{id:[0-9]+}/{email}', function (Request $request, Response $response, array $args) {
    
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $id = $args['id'];
    $email=$args['email'];
    $userId=DB::queryFirstRow("select id from users where email=%s and id<>%i",$email,$id);
    if($userId)
    {
       $response->getBody()->write(json_encode(true));
    }else
    {
       $response->getBody()->write(json_encode(false));
    }
    
    return $response;
 });

 $app->get('/api/seller/ordersInformation/{condition}', function (Request $request, Response $response, array $args) {
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $condition=$args['condition'];
        
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $ordersInfo=array();
        switch( $condition)
        {
            case 'today':
                $count=DB::queryFirstColumn("select count(ID) from orders where DATE(orderTime)=DATE(NOW()) and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where DATE(orderTime)=DATE(NOW()) and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and DATE(b.orderTime)=DATE(NOW()) and b.sellerId=".$seller["id"]);
                $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];
            break;
            case 'placed':

                $count=DB::queryFirstColumn("select count(ID) from orders where orderStatus='placed' and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where orderStatus='placed' and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and b.orderStatus='placed' and b.sellerId=".$seller["id"]);
                $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];
            break;
            case 'paid':
                $count=DB::queryFirstColumn("select count(ID) from orders where orderStatus='paid' and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where orderStatus='paid' and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and b.orderStatus='paid' and b.sellerId=".$seller["id"]);
                $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];
            break;
            case 'shipped':
                $count=DB::queryFirstColumn("select count(ID) from orders where orderStatus='shipped' and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where orderStatus='shipped' and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and b.orderStatus='shipped' and b.sellerId=".$seller["id"]);
                 $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];
            break;
            case 'month':
                $count=DB::queryFirstColumn("select count(ID) from orders where YEAR(orderTime)=YEAR(NOW()) and MONTH(orderTime)=MONTH(NOW()) and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where  YEAR(orderTime)=YEAR(NOW()) and MONTH(orderTime)=MONTH(NOW()) and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and  YEAR(b.orderTime)=YEAR(NOW()) and MONTH(b.orderTime)=MONTH(NOW()) and b.sellerId=".$seller["id"]);
                 $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];

            break;
            case 'year':
                $count=DB::queryFirstColumn("select count(ID) from orders where YEAR(orderTime)=YEAR(NOW()) and sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where  YEAR(orderTime)=YEAR(NOW()) and sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and  YEAR(b.orderTime)=YEAR(NOW()) and b.sellerId=".$seller["id"]);
                $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];

            break;
            case 'total':
                $count=DB::queryFirstColumn("select count(ID) from orders where sellerId=".$seller["id"]);
                $total=DB::queryFirstColumn("select COALESCE(SUM(total),0) from orders where sellerId=".$seller["id"]);
                $productCount=DB::queryFirstColumn("select COALESCE(SUM(a.buyCount),0) from orderitems a, orders b where a.orderId=b.id and b.sellerId=".$seller["id"]);
                 $ordersInfo=['count'=>$count,'total'=>$total,'productCount'=>$productCount];
            break;
            default:

        }
        $json = json_encode($ordersInfo, JSON_PRETTY_PRINT);
        $response->getBody()->write($json);
        return $response;
     }
    else
    {
     $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });

 $app->get('/api/seller/orders/{status}', function (Request $request, Response $response, array $args) {
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $status=$args['status'];
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b  where a.buyerId=b.id and a.orderStatus='".$status."' and a.sellerId=".$seller["id"]);
        $json = json_encode($orders, JSON_PRETTY_PRINT);
        $response->getBody()->write($json);
        return $response;
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });
 $app->get('/api/seller/orders/time/{period}/{inprocess}[/{startDate}/{endDate}]', function (Request $request, Response $response, array $args) {
    $seller=null;
    global $log;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $period=$args['period'];
        $inprocess=(int)$args['inprocess'];
        $statusCondition="";
     
        if($inprocess==0)
        {
            $statusCondition=" and a.orderStatus in ('delivered','returned') ";
        }
       
       $orders=array();
        switch($period)
        {
            case "month":
                 $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW()) and MONTH(a.orderTime)=MONTH(NOW())".$statusCondition." and a.sellerId=".$seller["id"]);
                
            break;
            case "quarter":
                $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW()) and QUARTER(a.orderTime)=QUARTER(NOW())".$statusCondition." and a.sellerId=".$seller["id"]);
            break;
            case "year":
                $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW())".$statusCondition." and a.sellerId=".$seller["id"]);

            break;
            case "anyTime":
                $startDate=$args['startDate'];
                $endDate=$args['endDate'];
                $orders=DB::query("select a.*,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName from orders a,users b where a.buyerId=b.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."'".$statusCondition." and a.sellerId=".$seller["id"]);
            break;
          
        }
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $json = json_encode($orders, JSON_PRETTY_PRINT);
        $response->getBody()->write($json);
        return $response;
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });
 $app->get('/api/seller/orderitems/{orderId}', function (Request $request, Response $response, array $args) {
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $orderId=$args['orderId'];
        
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
      
        $orderItems=DB::query("select a.*,b.name as productName,b.pictureUrl from orderitems a,products b,orders c where a.orderId=c.id and a.productId=b.id and a.orderId=". $orderId." and c.sellerId=".$seller["id"]);
        $json = json_encode($orderItems, JSON_PRETTY_PRINT);
        $response->getBody()->write($json);
        return $response;
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });

 $app->get('/api/seller/products/statistics/{period}[/{startDate}/{endDate}]', function (Request $request, Response $response, array $args) {
    $seller=null;
    global $log;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $period=$args['period'];
        $sql="";
      
        switch($period)
        {
            case "month":
                 $sql="select a.productId,b.name as productName,b.pictureUrl,DATE_FORMAT(c.orderTime,'%Y-%m-%d') AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                    "where a.productId=b.id and a.orderId=c.id and YEAR(c.orderTime)=YEAR(NOW()) and MONTH(c.orderTime)=MONTH(NOW()) and c.sellerId=".$seller["id"]."  group by a.productId,DATE_FORMAT(c.orderTime,'%Y-%m-%d')";
            break;
            case "quarter":
                $sql="select a.productId,b.name as productName,b.pictureUrl,MONTHNAME(c.orderTime) AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                    "where a.productId=b.id and a.orderId=c.id and YEAR(c.orderTime)=YEAR(NOW()) and QUARTER(c.orderTime)=QUARTER(NOW()) and c.sellerId=".$seller["id"]."  group by a.productId,MONTHNAME(c.orderTime)";
            break;
            case "year":
                $sql="select a.productId,b.name as productName,b.pictureUrl,MONTHNAME(c.orderTime) AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                "where a.productId=b.id and a.orderId=c.id and YEAR(c.orderTime)=YEAR(NOW()) and c.sellerId=".$seller["id"]."  group by a.productId,MONTHNAME(c.orderTime)";
            break;
            case "anyTime":
                $startDate=$args['startDate'];
                $endDate=$args['endDate'];
                $days  = $endDate->diff($startDate)->format('%a');
                if($days<=31)
                {
                    $sql="select a.productId,b.name as productName,b.pictureUrl,DATE_FORMAT(c.orderTime,'%Y-%m-%d') AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                    "where a.productId=b.id and a.orderId=c.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and c.sellerId=".$seller["id"]."  group by a.productId,DATE_FORMAT(c.orderTime,'%Y-%m-%d')";
                } 
                else if($days>31 && $days<=366)
                {
                    $sql="select a.productId,b.name as productName,b.pictureUrl,MONTHNAME(c.orderTime) AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                    "where a.productId=b.id and a.orderId=c.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and c.sellerId=".$seller["id"]."  group by a.productId,MONTHNAME(c.orderTime)";
                }else
                {
                    $sql="select a.productId,b.name as productName,b.pictureUrl,YEAR(c.orderTime) AS productPeriod,sum(a.buyCount) as totalCount,sum(a.subtotal) as totalSales from orderitems a, products b,orders c ".
                    "where a.productId=b.id and a.orderId=c.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and c.sellerId=".$seller["id"]."  group by a.productId,YEAR(c.orderTime)";
                }
             break;
          
        }
        if($sql!="")
        {
            $log->debug($sql);
            $products=DB::query($sql);
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $json = json_encode($products, JSON_PRETTY_PRINT);
            $response->getBody()->write($json);
            return $response;
        }
        else
        {
            $response->getBody()->write(json_encode(null));
        }
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });

 $app->get('/api/seller/sales/statistics/{period}[/{startDate}/{endDate}]', function (Request $request, Response $response, array $args) {
    $seller=null;
    global $log;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $period=$args['period'];
        $sql="";
      
        switch($period)
        {
            case "month":
                $sql="select DATE_FORMAT(a.orderTime,'%Y-%m-%d') AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where YEAR(a.orderTime)=YEAR(NOW()) and MONTH(a.orderTime)=MONTH(NOW()) and a.sellerId=".$seller["id"]."  group by DATE_FORMAT(a.orderTime,'%Y-%m-%d')";
            break;
            case "quarter":
                $sql="select MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where YEAR(a.orderTime)=YEAR(NOW()) and Quarter(a.orderTime)=QUARTER(NOW()) and a.sellerId=".$seller["id"]."  group by MONTHNAME(a.orderTime)";
            break;
            case "year":
                $sql="select MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where YEAR(a.orderTime)=YEAR(NOW()) and a.sellerId=".$seller["id"]."  group by MONTHNAME(a.orderTime)";
           break;
            case "anyTime":
                $startDate=$args['startDate'];
                $endDate=$args['endDate'];
                $days  = $endDate->diff($startDate)->format('%a');
                if($days<=31)
                {
                    $sql="select DATE_FORMAT(a.orderTime,'%Y-%m-%d') AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where  Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by DATE_FORMAT(a.orderTime,'%Y-%m-%d')";
                }
                else if($days>31 && $days<=366)
                {
                    $sql="select MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where  Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by MONTHNAME(a.orderTime)";
                }else
                {
                    $sql="select YEAR(a.orderTime) AS orderPeriod,sum(a.total) as salesTotal from orders a ".
                    "where Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by YEAR(a.orderTime)";
                }
            
             break;
          
        }
        if($sql!="")
        {
            $log->debug($sql);
            $products=DB::query($sql);
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $json = json_encode($products, JSON_PRETTY_PRINT);
            $response->getBody()->write($json);
            return $response;
        }
        else
        {
            $response->getBody()->write(json_encode(null));
        }
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });

 $app->get('/api/seller/customers/statistics/{period}[/{startDate}/{endDate}]', function (Request $request, Response $response, array $args) {
    $seller=null;
    global $log;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $period=$args['period'];
        $sql="";
      
        switch($period)
        {
            case "month":
                 $sql="select a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName, DATE_FORMAT(a.orderTime,'%Y-%m-%d') AS orderPeriod, sum(a.total) as buyTotal from orders a, users b ".
                    "where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW()) and MONTH(a.orderTime)=MONTH(NOW()) and a.sellerId=".$seller["id"]."  group by a.buyerId, DATE_FORMAT(a.orderTime,'%Y-%m-%d')";
            break;
            case "quarter":
                $sql="select a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName,MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as buyTotal from orders a, users b ".
                    "where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW()) and Quarter(a.orderTime)=QUARTER(NOW()) and a.sellerId=".$seller["id"]."  group by a.buyerId,MONTHNAME(a.orderTime)";
            break;
            case "year":
                $sql="select a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName,MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as buyTotal from orders a, users b  ".
                    "where a.buyerId=b.id and YEAR(a.orderTime)=YEAR(NOW()) and a.sellerId=".$seller["id"]."  group by a.buyerId,MONTHNAME(a.orderTime)";
           break;
            case "anyTime":
                $startDate=$args['startDate'];
                $endDate=$args['endDate'];
                $days  = $endDate->diff($startDate)->format('%a');
                if($days<=31)
                {
                    $sql="select a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName,DATE_FORMAT(a.orderTime,'%Y-%m-%d') AS orderPeriod,sum(a.total) as buyTotal from orders a, users b ".
                    "where a.buyerId=b.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by a.buyerId,DATE_FORMAT(a.orderTime,'%Y-%m-%d')";
                }
                else if($days>31 && $days<=366)
                {
                    $sql="select a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName,MONTHNAME(a.orderTime) AS orderPeriod,sum(a.total) as buyTotal from orders a, users b ".
                    "where a.buyerId=b.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by a.buyerId,MONTHNAME(a.orderTime)";
                }else
                {
                    $sql="select  a.buyerId,CONCAT_WS(' ', b.FirstName, b.LastName) AS buyerName,YEAR(a.orderTime) AS orderPeriod,sum(a.total) as buyTotal from orders a, users b ".
                    "where a.buyerId=b.id and Date(a.orderTime)>='".$startDate."' and  Date(a.orderTime)<='".$endDate."' and a.sellerId=".$seller["id"]."  group by a.buyerId, YEAR(a.orderTime)";
                }
            
             break;
          
        }
        if($sql!="")
        {
            $log->debug($sql);
            $products=DB::query($sql);
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $json = json_encode($products, JSON_PRETTY_PRINT);
            $response->getBody()->write($json);
            return $response;
        }
        else
        {
            $response->getBody()->write(json_encode(null));
        }
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });

 $app->put('/api/seller/orders/{id}', function (Request $request, Response $response, array $args) {
    $seller=null;
    if(isset($_SESSION['seller']))
    {
        $seller=$_SESSION['seller'];
        $id=$args['id'];
        $json = $request->getBody();
        $changes = json_decode($json, true);
        $order=DB::queryFirstRow("select * from orders where id=".$id." and sellerId=".$seller["id"]);
        if(!$order)
        {
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
        DB::update('orders', $changes, "id=%i", $id);
        $response->getBody()->write(json_encode(true)); 
        return $response;
      
     }
    else
    {
        $response->getBody()->write(json_encode(null));
    }
  
    
    return $response;
 });
 $app->map(['PUT', 'PATCH'], '/api/seller/personalsetting/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    if(isset($_SESSION['seller']))
    {
  
        $id = $args['id'];
        if($id!=$_SESSION['seller']['id'])
        {
            $response = $response->withStatus(400);
            $response->getBody()->write(json_encode("400 -Action Forbidden"));
            return $response; 
        }
        $json = $request->getBody();
        $updateField= json_decode($json, true);
        $updatePart=$updateField['update-type'];
        $errorList = array();
        switch($updatePart)
        {
            case "register":
               $firstname= $updateField['firstname'];
               $middlename=$updateField['middlename'];
               $lastname=$updateField['lastname'];
               $phone=$updateField['phone'];
               if(strlen($firstname)<1||strlen($firstname>50))
               {
                   array_push($errorList, "First name must be 1-50 charactors."); 
               }
           
               if(strlen($lastname)<1||strlen($lastname>50))
               {
                   array_push($errorList, "Last name must be 1-50 charactors."); 
               }
               if(strlen($middlename)<0||strlen($lastname>10))
               {
                   array_push($errorList, "Middle name must be 0-10 charactors."); 
               }
               if($errorList)
               {
                    //$response = $response->withStatus(400);
                    $response->getBody()->write(json_encode($errorList));
                    return $response;
               }
               else
               {
                DB::update('users', ['FirstName' => $firstname, 'MiddleName' =>$middlename,'LastName'=>$lastname,'phoneNo'=>$phone], "id=%i", $id);
                $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
                return $response;
               }
            break;
            case "address":
                $address= $updateField['address'];
                $city=$updateField['city'];
                $province=$updateField['province'];
                $country=$updateField['country'];
                $postalcode=$updateField['postalcode'];

                DB::update('users', ['address' => $address, 'city' =>$city,'province'=>$province,'country'=>$country,'postalCode'=>$postalcode], "id=%i", $id);
                $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
                return $response;
            break;
            case "description":
                $description= $updateField['description'];
             

                DB::update('users', ['description' => $description], "id=%i", $id);
                $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
                return $response;
            break;
            case "password":
                $seller=DB::queryFirstRow("select * from users where id=".$id);
                $oldPassword= $updateField['oldpassword'];  
                $newPassword= $updateField['newpassword'];    
                $confirm= $updateField['confirm'];
                if($oldPassword!=$seller['password'])
                {

                    array_push($errorList, "Old password error.");     
                }
                if ($newPassword != $confirm) {
                    array_push($errorList, "New passwords do not match");        
                } 
                else 
                {
                    if ((strlen($newPassword) < 6)
                            || (preg_match("/[A-Z]/", $newPassword) == FALSE )
                            || (preg_match("/[a-z]/", $newPassword) == FALSE )
                            || (preg_match("/[0-9]/", $newPassword) == FALSE )) 
                    {
                        array_push($errorList, "Password must be at least 6 characters long, "
                                . "with at least one uppercase, one lowercase, and one digit in it");
                    }
                } 
                if($errorList)
                {
                     //$response = $response->withStatus(400);
                     $response->getBody()->write(json_encode($errorList));
                     return $response;
                }
                else
                {
                 DB::update('users', ['password' => $newPassword], "id=%i", $id);
                 $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
                 return $response;
                }

            break; 


        }
     
    }else
    {

        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - Not login"));
        return $response;
    }
});


 
?>