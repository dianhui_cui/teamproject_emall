<?php
require "setup.php";
require_once "account.php";
require_once "admin.php";
require_once "buyer.php";
require_once "seller.php";

use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;




$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $name=isset($_SESSION['user'])? $_SESSION['user']['username'] : "";
    $categoryList = DB::query("SELECT * FROM productcategories WHERE parentId IS NULL ORDER BY id");
    $productList = DB::query(
        "SELECT p.* FROM products p 
        ORDER BY p.salesCount LIMIT %i ", 6);
    foreach($categoryList as &$category) {
        $subCategoryList = DB::query("SELECT id, name FROM productcategories WHERE parentId = %i", $category['id']);
        $category['subCategoyList'] = $subCategoryList;
        //print_r($category);
    }
    //print_r($categoryList);
    foreach($productList as &$product){
        $product['picture'] = base64_encode($product['picture']);
    }
    return $view->render($response, 'index.html.twig', ['categoryList' => $categoryList, 'productList' => $productList]);
});
$app->get('/test', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $user=null;
    if(isset($_SESSION['user']))
    {
        $user=$_SESSION['user'];
    }
    return $view->render($response, 'test-index.html.twig',['user'=>$user]);
});
$app->get('/products/list/{categoryid:[0-9]+}', function ($request, $response, $args) use ($app){
    $view = Twig::fromRequest($request);
    $categoryId = $args['categoryid'];
    $category = DB::queryFirstRow("SELECT * from productcategories WHERE id=%i", $categoryId);
    $subCategoryList = DB::query("SELECT * from productcategories WHERE parentId=%i;", $categoryId);
    $productList = array();
    
    
    return $view->render($response, 'products_list.html.twig',['subCategoryList'=>$subCategoryList, 'category' => $category]);
});
const PRODUCTS_PER_PAGE = 5;
$app->get('/ajax/productlist/{categoryId:[0-9]+}/{pageNo:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $pageNo = $args['pageNo'];
    $categoryId = $args['categoryId'];
    $totalProducts = 0;
    $totalPages = 0;
    $productSkip = ($pageNo - 1) * PRODUCTS_PER_PAGE;
    
    $subCategoryList = DB::query("SELECT * from productcategories WHERE parentId=%i;", $categoryId);
    if (count($subCategoryList)!=0){
        /*foreach($subCategoryList as $subCategory) {
            $productList_subcategory = DB::query("SELECT * from products WHERE categoryId=%i", $subCategory['id']);
            if (count($productList_subcategory)!= 0 && count){
                array_push($productList, $productList_subcategory);
            }
        }*/
        $totalProducts = DB::queryFirstField("
        SELECT COUNT(*) AS 'count' FROM products p 
        INNER JOIN productcategories pc  
        WHERE p.categoryId=pc.id AND pc.parentId=%i ", $categoryId);

        $productList = DB::query(
            "SELECT p.* FROM products p 
            INNER JOIN productcategories pc  
            WHERE p.categoryId=pc.id AND pc.parentId=%i  ORDER BY p.id LIMIT %i,%i ",$categoryId, $productSkip, PRODUCTS_PER_PAGE);
    } else {
        $totalProducts = DB::queryFirstField("SELECT COUNT(*) AS 'count' FROM products WHERE categoryId = %d", $categoryId);
        $productList = DB::query("SELECT * from products WHERE categoryId=%i ORDER BY id LIMIT %i,%i ", $categoryId, $productSkip, PRODUCTS_PER_PAGE);
    }
    // print_r($productList);
    //$productList = DB::query("SELECT * FROM products WHERE categoryId = %d ORDER BY id LIMIT %i,%i ", $categoryId, $productSkip, PRODUCTS_PER_PAGE);    
    // if encode the image by base64 when upload, this will not be needed
    foreach($productList as &$product){
        $product['picture'] = base64_encode($product['picture']);
    }
    $totalPages = ceil($totalProducts / PRODUCTS_PER_PAGE);
    return $view->render($response, '/product_list_load.html.twig', [ 'productList' => $productList, 'totalPages' => $totalPages, 'categoryId' => $categoryId ]);
});

$app->run();