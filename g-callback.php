<?php
	require_once "config.php";

	if (isset($_SESSION['access_token']))
		$gClient->setAccessToken($_SESSION['access_token']);
	else if (isset($_GET['code'])) {
		$token = $gClient->fetchAccessTokenWithAuthCode($_GET['code']);
		$_SESSION['access_token'] = $token;
	} else {
		header('Location: /login');
		exit();
	}

	$oAuth = new Google_Service_Oauth2($gClient);
	$userData = $oAuth->userinfo_v2_me->get();

	$_SESSION['guser']['id'] = $userData['id'];
	$_SESSION['guser']['email'] = $userData['email'];
	$_SESSION['guser']['gender'] = $userData['gender'];
	$_SESSION['guser']['picture'] = $userData['picture'];
	$_SESSION['guser']['familyName'] = $userData['familyName'];
	$_SESSION['guser']['givenName'] = $userData['givenName'];

	header('Location: /');
	exit();
?>