<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;

use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require __DIR__ . '/vendor/autoload.php';

session_start();


// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));//threshhold, DEBUG=100 and will put everything severrer to log
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

// setup db  
if (strpos($_SERVER['HTTP_HOST'],"ipd20.com") !== false){

    DB::$user = 'cp4966_teamproject_emall';
	DB::$password = '5lM(Z50RSzEI';
    DB::$dbName = 'cp4966_teamproject_emall';
} else {

    
    DB::$user = 'cp4966_teamproject_emall';
	DB::$password = '5lM(Z50RSzEI';
	DB::$dbName = 'cp4966_teamproject_emall';
    DB::$port = 3333;
}
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)
function db_error_handler($params) {
    //redirect
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])){
        $log->error("SQL query:" . $params['query']);
    }
    die;
    echo "Error: " . $params['error'] . "<br>\n";
    echo "Query: " . $params['query'] . "<br>\n";
    die; // don't want to keep going if a query broke
}

$container = new Container();
$container->set('upload_directory', __DIR__ . '/uploads');
AppFactory::setContainer($container);


$app = AppFactory::create();

// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            return $response->withHeader('Location','/error_notfound', 404);
        }
);
// Add Error Middleware
//$errorMiddleware = $app->addErrorMiddleware(true, true, true);
//$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $forbiddenErrorHandler);

  
// Create Twig
//$path=(__DIR__.'/templates'.'|'.__DIR__.'/cache');
//$log->error($path);
$twig = Twig::create(__DIR__.'/templates', ['cache' => __DIR__.'/cache','debug' => true]);
// Set Global variable($_SESSION['user'])
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false);
$twig->getEnvironment()->addGlobal('googleUserSession', isset($_SESSION['access_token']) ? $_SESSION['guser'] : false);
// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});
$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});
function moveUploadedFileRandom($directory, UploadedFileInterface $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}
?>